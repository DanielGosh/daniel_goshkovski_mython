#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include "Boolean.h"
#include "String.h"
#include "Integer.h"
#include "IndentationException.h"
class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);

private:




};

#endif //PARSER_H
