#include "Boolean.h"

Boolean::Boolean(bool var, bool isTemp)
{
	this->_isTemp = isTemp;
	this->_var = var;
}

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return this->_var ? "true" : "false";
}
