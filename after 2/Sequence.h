#ifndef SEQUENCE_H
#define SEQUENCE_H


#include "type.h"
#include <iostream>
class Sequence : public Type
{
protected:
public:
	bool getIsTemp();
	void setIsTemp(bool isTemp);
	bool isPrintable() const;
	std::string toString() const;

};
#endif // SEQUENCE_H
