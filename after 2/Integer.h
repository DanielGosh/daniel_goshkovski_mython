#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <iostream>

class Integer : public Type
{
protected:
	int _var;
public:
	Integer(int var, bool isTemp = false);
	bool getIsTemp();
	void setIsTemp(bool isTemp);
	bool isPrintable() const;
	std::string toString() const;

};

#endif // INTEGER_H