#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"
#include <iostream>

class Boolean: public Type
{
protected:
	bool _var;
public:
	Boolean(bool var, bool isTemp = false);
	bool getIsTemp();
	void setIsTemp(bool isTemp);
	bool isPrintable() const;
	std::string toString() const;
};

#endif // BOOLEAN_H