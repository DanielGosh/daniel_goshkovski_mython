#ifndef TYPE_H
#define TYPE_H
#include <iostream>
#include <string>
class Type
{
protected:
	bool _isTemp;
public:
	Type();
	bool getIsTemp();
	void setIsTemp(bool isTemp);
	virtual bool isPrintable() const = 0;
	std::string toString() const;

};

#endif //TYPE_H
