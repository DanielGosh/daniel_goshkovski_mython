#include "parser.h"
#include <iostream>
#include "IndentationException.h"

Type* Parser::parseString(std::string str) throw()
{
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
		{
			throw IndentationException();
		}
		std::cout << str << std::endl;
	}

	return NULL;
}


