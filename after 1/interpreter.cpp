#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "[DANIEL GOSHKOVSKI]"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	try {
		while (input_string != "quit()")
		{
				// prasing command
			Parser::parseString(input_string);
				// get new command from user
			std::cout << ">>> ";
			std::getline(std::cin, input_string);
			
		}
	}
	catch (InterperterException& e) {
			std::cout << e.what();
	}

	return 0;
}


